package com.daniel.texturas;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Triangulo extends Geometria {


    public Triangulo(float largura, float altura, GL10 gl10, float proporcao) {
        super(largura, altura, gl10, proporcao);
    }

    public Triangulo() {
        super();
    }

    @Override
    public void desenha() {

        float refX = 100;
        float refY = 100;


        float[] vetCoords1 =
                {
                        proporcao, (refY)*proporcao,
                        (-refX*2)*proporcao, (-refY)*proporcao,
                        (refX*2)*proporcao, (-refY)*proporcao
                };

        //Cria o vetor de coordenadas
        FloatBuffer buffer1 = generateBuffer(vetCoords1);

        //Registra as coordenadas na maquina OpenGL
        gl10.glVertexPointer(2,
                GL10.GL_FLOAT,
                0, buffer1);

        gl10.glTranslatef(posicaoX, posicaoY,0);
        gl10.glRotatef(anguloRotacao+45, 0,0, anguloZ);
        //Transladar um objeto
        gl10.glColor4f(redColor, greenColor, blueColor, 1.0f);
        gl10.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 3);

        gl10.glLoadIdentity();
    }
}
