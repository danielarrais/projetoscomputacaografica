package com.daniel.novoappcomputacaografica;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


/*****************************************************************************
 * Programa: Exemplo3
 * Descricao: Demonstra como utilizar OpenGL para desenhar primitivas em Android
 * Autor: Silvano Malfatti
 * Local: Palmas-TO
 *****************************************************************************/

//Pacote da aplicacao (identificador unico)

//Classe principal da aplicacao
public class TelaPrincipal extends Activity {
    //Atrivutos da classe
    private GLSurfaceView prDrawSurface = null;
    private OpenglRender prRenderer = null;

    private SensorManager mSensorManager;
    private Sensor mAcelerometro;

    //Metodo sobrescrito da classe Activity
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prRenderer = new OpenglRender();
        prDrawSurface = new GLSurfaceView(this);
        prDrawSurface.setRenderer(prRenderer);
        //Registra o OnTouchLitener na superficie
        prDrawSurface.setOnTouchListener(prRenderer);

        setContentView(prDrawSurface);

//        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//        mAcelerometro = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mSensorManager.registerListener(prRenderer, mAcelerometro, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mSensorManager.unregisterListener(prRenderer);
    }
}
