package com.daniel.novoappcomputacaografica;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public abstract class Geometria {

    protected float posicaoX;
    protected float posicaoY;

    protected float largura;
    protected float altura;

    protected float refX = 100;
    protected float refY = 100;

    protected float larguraFigura;
    protected float alturaFigura;


    protected float anguloZ;

    protected float greenColor;
    protected float redColor;
    protected float blueColor;

    protected float proporcao;

    protected float anguloRotacao;

    protected GL10 gl10;

    public Geometria(float largura, float altura, GL10 gl10, float proporcao) {
        setAllProperties(largura, altura, gl10, proporcao);
    }

    public Geometria() {

    }

    public void setAllProperties(float largura, float altura, GL10 gl10, float proporcao) {
        this.largura = largura;
        this.altura = altura;
        this.gl10 = gl10;
        this.proporcao = proporcao;
    }

    /**
     * Método que define as posição da forma
     * @param posicaoX Posição X
     * @param posicaoY Posição Y
     */
    public void setPosicao(float posicaoX, float posicaoY) {
        this.posicaoX = posicaoX;
        this.posicaoY = posicaoY;
    }

    /**
     * Método que define as cores do forma
     *
     * @param greenColor Cor verde
     * @param redColor Cor vermelha
     * @param blueColor Cor azul
     */
    public void setCores(float greenColor, float redColor, float blueColor) {
        this.greenColor = greenColor;
        this.redColor = redColor;
        this.blueColor = blueColor;
    }

    public void setRotacao(float anguloRotacao, float anguloZ) {
        this.anguloZ = anguloZ;
        this.anguloRotacao = anguloRotacao;
    }

    /**
     * Método que introduz a forma na tela de desenho
     */
    public abstract void desenha();


    FloatBuffer generateBuffer(float[] vetor)
    {
        ByteBuffer prBuffer = //aloca memoria em bytes para os vertices
                ByteBuffer.
                        allocateDirect(vetor.length
                                * 4);

        //Ordena os enderecos de memoria conforme
        // a arquitetura do  processador
        prBuffer.order(ByteOrder.nativeOrder());

        //Gera o encapsulador, limpa, insere o vetor java
        //Retira eventuais sobras de memoria, retorna
        FloatBuffer prFloat =
                prBuffer.asFloatBuffer();
        prFloat.clear();
        prFloat.put(vetor);
        prFloat.flip();

        return prFloat;
    }

    public float getPosicaoX() {
        return posicaoX;
    }

    public void setPosicaoX(float posicaoX) {
        this.posicaoX = posicaoX;
    }

    public float getPosicaoY() {
        return posicaoY;
    }

    public void setPosicaoY(float posicaoY) {
        this.posicaoY = posicaoY;
    }
}
